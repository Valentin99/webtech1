$.getJSON("http://webtechcars.herokuapp.com/api/manufacturers",
    function (manufactures) {

        let dropdown = document.getElementById("manufacturer");
        let option;

        for (var i = 0; i < manufactures.length; i++) {
            option = document.createElement("option");
            option.value = manufactures[i].name;
            option.text = manufactures[i].name;
            dropdown.appendChild(option);
        }

    });

$("#btnCreate").click(function (event) {
    event.preventDefault();

    var data = {};

    data.name = document.getElementById("name").value;
    data.manufacturer = document.getElementById("manufacturer").value;
    data.consumption = document.getElementById("consumption").value;
    data.color = document.getElementById("color").value;
    data.avaiable = document.getElementById("available").value;
    data.year = document.getElementById("year").value;
    data.horsepower = document.getElementById("horsepower").value;

    var res = Validate();

    if (res === true) 
    {

        console.log("itt");

        $.ajax({
            type: 'POST',
            url: "https://webtechcars.herokuapp.com/api/cars",
            data: JSON.stringify(data),
            statusCode: {
                201: handle201,
            },
            dataType: "json",
            contentType: "application/json",
        });


        document.getElementById("name").value = null;
        document.getElementById("manufacturer").value = null;
        document.getElementById("consumption").value = null;
        document.getElementById("color").value = null;
        document.getElementById("available").value = null;
        document.getElementById("year").value = null;
        document.getElementById("horsepower").value = null;
    } else {
        return false;
    }


});

function handle201() {
    alert('Létrehozás sikeres.');
    window.location.href = '../../index.html'

};

function Validate() {

    var isValid = true;

    $('#nameError').html("");
    $('#manufacturerError').html("");
    $('#consumptionError').html("");
    $('#colorError').html("");
    $('#availableError').html("");
    $('#yearError').html("");
    $('#horsepowerError').html("");

    if ($('#name').val().length > 20) {
        $('#name').css('border-color', 'Red');
        $('#nameError').html("Maximum hossz 20 karakter!");
        $('#nameError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#name').css('border-color', 'lightgrey');
    }

    if ($('#name').val().trim() == "") {
        $('#name').css('border-color', 'Red');
        $('#nameError').html("A mező kitöltése kötelező");
        $('#nameError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#name').css('border-color', 'lightgrey');
    }

    if ($('#manufacturer').val().trim() == "Gyarto") {
        $('#manufacturer').css('border-color', 'Red');
        $('#manufacturerError').html("A mező kitöltése kötelező");
        $('#manufacturerError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#manufacturer').css('border-color', 'lightgrey');
    }

    if ($('#consumption').val().length > 20) {
        $('#consumption').css('border-color', 'Red');
        $('#consumptionError').html("Maximum hossz 20 karakter!");
        $('#consumptionError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#consumption').css('border-color', 'lightgrey');
    }

    if ($('#consumption').val().trim() == "") {
        $('#consumption').css('border-color', 'Red');
        $('#consumptionError').html("A mező kitöltése kötelező");
        $('#consumptionError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#consumption').css('border-color', 'lightgrey');
    }

    if ($('#color').val().length > 20) {
        $('#color').css('border-color', 'Red');
        $('#colorError').html("Maximum hossz 20 karakter!");
        $('#colorError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#color').css('border-color', 'lightgrey');
    }

    if ($('#color').val().trim() == "") {
        $('#color').css('border-color', 'Red');
        $('#colorError').html("A mező kitöltése kötelező");
        $('#colorError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#color').css('border-color', 'lightgrey');
    }

    if ($('#year').val() < 0) {
        $('year').css('border-color', 'Red');
        $('#yearError').html("A mező nem lehet negatív érték!");
        $('#yearError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#year').css('border-color', 'lightgrey');
    }

    return isValid;
}  