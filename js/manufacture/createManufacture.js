$("#btnCreate").click(function (event) {

    var data = {};

    data.name = document.getElementById("name").value;
    data.country = document.getElementById("country").value;
    data.founded = document.getElementById("founded").value;

    var res = Validate();

    if (res == true) {

        $.ajax({
            type: 'POST',
            url: "https://webtechcars.herokuapp.com/api/manufacturers",
            data: JSON.stringify(data),
            statusCode: {
                201: handle201,
            },
            error: function (e) {
                console.log(e);
            },
            dataType: "json",
            contentType: "application/json",
        });

        event.preventDefault();

        document.getElementById("name").value = null;
        document.getElementById("country").value = null;
        document.getElementById("founded").value = null;
    }
    else {
        return false;
    }

});

function ErrorReload() {
    alert('A megadott értékek nem megfelelök!');
    window.location.href = 'createManufacture.html'
}

var handle201 = function () {
    alert('Létrehozás sikeres.');
    window.location.href = 'manufacture.html'
};

function Validate() {

    var isValid = true;
    
    $('#nameError').html("");
    $('#countryError').html("");
    $('#foundedError').html("");
    
    if($('#name').val().length > 20) {
        $('#name').css('border-color', 'Red');
        $('#nameError').html("Maximum hossz 20 karakter!");
        $('#nameError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#name').css('border-color', 'lightgrey');
    } 
    
    if ($('#name').val().trim() == "") {
        $('#name').css('border-color', 'Red');
        $('#nameError').html("A mező kitöltése kötelező");
        $('#nameError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#name').css('border-color', 'lightgrey');
    }
    
    if ($('#country').val().trim() == "") {
        $('#country').css('border-color', 'Red');
        $('#countryError').html("A mező kitöltése kötelező");
        $('#countryError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#country').css('border-color', 'lightgrey');
    }

    if($('#country').val().length > 20) {
        $('#country').css('border-color', 'Red');
        $('#countryError').html("Maximum hossz 20 karakter!");
        $('#countryError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#country').css('border-color', 'lightgrey');
    } 
    
    if ($('#founded').val().trim() == "") {
        $('#founded').css('border-color', 'Red');
        $('#foundedError').html("A mező kitöltése kötelező");
        $('#foundedError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#founded').css('border-color', 'lightgrey');
    }

    if($('#founded').val() < 0) {
        $('founded').css('border-color', 'Red');
        $('#foundedError').html("A mező nem lehet negatív érték!");
        $('#foundedError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#founded').css('border-color', 'lightgrey');
    } 

    return isValid;
}  