window.onload = function () {
    document.getElementById("name").value = localStorage.getItem('name');
    document.getElementById("manufacturer").value = localStorage.getItem('manufacturer');
    document.getElementById("consumption").value = localStorage.getItem('consumption');
    document.getElementById("color").value = localStorage.getItem('color');
    document.getElementById("avaiable").value = Number(localStorage.getItem('avaiable'));
    document.getElementById("year").value = Number(localStorage.getItem('year'));
    document.getElementById("horsepower").value = Number(localStorage.getItem('horsepower'));
}

$.getJSON("http://webtechcars.herokuapp.com/api/manufacturers",
    function (manufactures) {

        let dropdown = document.getElementById("manufacturer");
        let option;
        option = document.createElement("option");
        option.value = localStorage.getItem('manufacturer');
        option.text = localStorage.getItem('manufacturer');
        dropdown.appendChild(option);
        for (var i = 0; i < manufactures.length; i++) {
            option = document.createElement("option");
            option.value = manufactures[i].name;
            option.text = manufactures[i].name;
            dropdown.appendChild(option);
        }
    });

$("#btnEdit").click(function (event) {
    event.preventDefault();

    let id = localStorage.getItem("id");

    var data = {};

    data._id = id;
    data.name = document.getElementById("name").value;
    data.manufacturer = document.getElementById("manufacturer").value;
    data.consumption = document.getElementById("consumption").value;
    data.color = document.getElementById("color").value;
    data.avaiable = document.getElementById("avaiable").value;
    data.year = document.getElementById("year").value;
    data.horsepower = document.getElementById("horsepower").value;

    let res = Validate();

    if (res === true) {
        deleteCar(id);
    } else {
        return false;
    }
    
});
function deleteCar(id) {

    var data = {};
    let url = `https://webtechcars.herokuapp.com/api/cars/${id}`;
    $.ajax({
        url: url,
        type: 'DELETE',
        data: JSON.stringify(data),
        error: function (e) {
            console.log(e);
        },
        dataType: "json",
        contentType: "application/json",
        statusCode: {
            200: handle200,
        },
    });

}
function create() {

    var data = {};

    data.name = document.getElementById("name").value;
    data.manufacturer = document.getElementById("manufacturer").value;
    data.consumption = document.getElementById("consumption").value;
    data.color = document.getElementById("color").value;
    data.avaiable = document.getElementById("avaiable").value;
    data.year = document.getElementById("year").value;
    data.horsepower = document.getElementById("horsepower").value;

    $.ajax({
        type: 'POST',
        url: "https://webtechcars.herokuapp.com/api/cars",
        data: JSON.stringify(data),
        statusCode: {
            201: handle201,
        },
        dataType: "json",
        contentType: "application/json",
    });
};
var handle201 = function () {
    alert('Módosítás sikeres.');
    window.location.href = '../../index.html'
};

var handle200 = function () {
    create();
};

function Validate() {

    var isValid = true;

    $('#nameError').html("");
    $('#manufacturerError').html("");
    $('#consumptionError').html("");
    $('#colorError').html("");
    $('#availableError').html("");
    $('#yearError').html("");
    $('#horsepowerError').html("");

    if ($('#name').val().length > 20) {
        $('#name').css('border-color', 'Red');
        $('#nameError').html("Maximum hossz 20 karakter!");
        $('#nameError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#name').css('border-color', 'lightgrey');
    }

    if ($('#name').val().trim() == "") {
        $('#name').css('border-color', 'Red');
        $('#nameError').html("A mező kitöltése kötelező");
        $('#nameError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#name').css('border-color', 'lightgrey');
    }

    if ($('#manufacturer').val().trim() == "Gyarto") {
        $('#manufacturer').css('border-color', 'Red');
        $('#manufacturerError').html("A mező kitöltése kötelező");
        $('#manufacturerError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#manufacturer').css('border-color', 'lightgrey');
    }

    if ($('#consumption').val().length > 20) {
        $('#consumption').css('border-color', 'Red');
        $('#consumptionError').html("Maximum hossz 20 karakter!");
        $('#consumptionError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#consumption').css('border-color', 'lightgrey');
    }

    if ($('#consumption').val().trim() == "") {
        $('#consumption').css('border-color', 'Red');
        $('#consumptionError').html("A mező kitöltése kötelező");
        $('#consumptionError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#consumption').css('border-color', 'lightgrey');
    }

    if ($('#color').val().length > 20) {
        $('#color').css('border-color', 'Red');
        $('#colorError').html("Maximum hossz 20 karakter!");
        $('#colorError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#color').css('border-color', 'lightgrey');
    }

    if ($('#color').val().trim() == "") {
        $('#color').css('border-color', 'Red');
        $('#colorError').html("A mező kitöltése kötelező");
        $('#colorError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#color').css('border-color', 'lightgrey');
    }

    if ($('#year').val() < 0) {
        $('year').css('border-color', 'Red');
        $('#yearError').html("A mező nem lehet negatív érték!");
        $('#yearError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#year').css('border-color', 'lightgrey');
    }

    return isValid;
}  