window.onload = function () {
    document.getElementById("name").value = localStorage.getItem('name');
    document.getElementById("country").value = localStorage.getItem('country');
    document.getElementById("founded").value = localStorage.getItem('founded');
}

$("#btnEdit").click(function (event) {
    event.preventDefault();
    let id = localStorage.getItem('id');
    var data = {};
    data._id = id;
    data.name = document.getElementById("name").value;
    data.country = document.getElementById("country").value;
    data.founded = document.getElementById("founded").value;

    Validate(id);
});

function create() {
    var data = {};

    data.name = document.getElementById("name").value;
    data.country = document.getElementById("country").value;
    data.founded = document.getElementById("founded").value;

    $.ajax({
        type: 'POST',
        url: "https://webtechcars.herokuapp.com/api/manufacturers",
        data: JSON.stringify(data),
        error: function (e) {
            console.log(e);
        },
        dataType: "json",
        contentType: "application/json",
        statusCode: {
            201: handle201,
        },
    });

    document.getElementById("name").value = null;
    document.getElementById("country").value = null;
    document.getElementById("founded").value = null;
};

var handle201 = function () {
    alert('Módosítás sikeres.');
    window.location.href = 'manufacture.html'
};

function deleteManufacture(id) {
    var data = {};
    let url = `https://webtechcars.herokuapp.com/api/manufacturers/${id}`;
    $.ajax({
        url: url,
        type: 'DELETE',
        data: JSON.stringify(data),
        error: function (e) {
            console.log(e);
        },
        dataType: "json",
        contentType: "application/json",
        statusCode: {
            200: handle200,
        },
    });
}

var handle200 = function () {
    create();
};

function Validate(id) {

    var isValid = true;
    $('#nameError').html("");
    $('#countryError').html("");
    $('#foundedError').html("");
    
    if($('#name').val().length > 20) {
        $('#name').css('border-color', 'Red');
        $('#nameError').html("Maximum hossz 20 karakter!");
        $('#nameError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#name').css('border-color', 'lightgrey');
    } 
    
    if ($('#name').val().trim() == "") {
        $('#name').css('border-color', 'Red');
        $('#nameError').html("A mező kitöltése kötelező");
        $('#nameError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#name').css('border-color', 'lightgrey');
    }
    
    if ($('#country').val().trim() == "") {
        $('#country').css('border-color', 'Red');
        $('#countryError').html("A mező kitöltése kötelező");
        $('#countryError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#country').css('border-color', 'lightgrey');
    }

    if($('#country').val().length > 20) {
        $('#country').css('border-color', 'Red');
        $('#countryError').html("Maximum hossz 20 karakter!");
        $('#countryError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#country').css('border-color', 'lightgrey');
    } 
    
    if ($('#founded').val().trim() == "") {
        $('#founded').css('border-color', 'Red');
        $('#foundedError').html("A mező kitöltése kötelező");
        $('#foundedError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#founded').css('border-color', 'lightgrey');
    }

    if($('#founded').val() < 0) {
        $('founded').css('border-color', 'Red');
        $('#foundedError').html("A mező nem lehet negatív érték!");
        $('#foundedError').css('color', 'Red');
        isValid = false;
    }
    else {
        $('#founded').css('border-color', 'lightgrey');
    } 

    if(isValid === true) {
        deleteManufacture(id);
    }

    return isValid;
}  